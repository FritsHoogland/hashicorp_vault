vault auth enable userpass
vault write auth/userpass/users/test_read_user password=mypass

vault secrets enable kv-v2
vault kv put kv-v2/test/demo foo=bar ping=pong
vault policy write test_read_kv-v2 policy_test_read_kv-v2.hcl
vault write auth/userpass/users/test_read_user policies="test_read_kv-v2"

vault secrets enable kv
vault kv put kv/test/demo bar=foo pong=ping
vault kv get kv/test/demo
vault policy write test_read_kv policy_test_read_kv.hcl
vault write auth/userpass/users/test_read_user policies="test_read_kv-v2,test_read_kv"

vault auth enable cert
-- openssl req -newkey rsa:2048 -new -nodes -x509 -days 365 -keyout demo_key.pem -out demo_cert.pem 
vault write auth/cert/certs/demo display_name=demo policies="test_read_kv-v2,test_read_kv" certificate=@demo_cert.pem 
