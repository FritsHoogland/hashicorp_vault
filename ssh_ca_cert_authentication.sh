vault secrets enable -path=ssh-client ssh
vault write ssh-client/config/ca generate_signing_key=true
vault read -field=public_key ssh-client/config/ca > trusted-user-ca-keys.pem
#
# /etc/ssh/sshd_config
# TrustedUserCAKeys trusted-user-ca-keys.pem
#
echo "{
  \"allow_user_certificates\": true,
  \"allowed_users\": \"*\",
  \"default_extensions\": [
    {
      \"permit-pty\": \"\"
    }
  ],
  \"key_type\": \"ca\",
  \"default_user\": \"vagrant\",
  \"ttl\": \"24h\",
  \"key_id_format\": \"{{token_display_name}}\"
}" | vault write ssh-client/roles/sign-role -
# ssh-keygen -t rsa -C "frits.hoogland@accenture.com"
vault write -field=signed_key ssh-client/sign/sign-role public_key=@$HOME/.ssh/id_rsa.pub > $HOME/.ssh/cert-signed.pub
# signed_key = signed public key

